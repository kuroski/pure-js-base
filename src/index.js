import './rectangle'
import api from './api'

// const searchKuroski = async () => {
//   const { data } = await getUser('kuroski')
//   const name = document.querySelector('.name')
//   const avatar = document.querySelector('.avatar')

//   name.innerHTML = data.name
//   avatar.setAttribute('src', data.avatar_url)
// }

// searchKuroski()

api.getUser('kuroski')
  .then(result => result.data)
  .then(data => {
    const name = document.querySelector('.name')
    const avatar = document.querySelector('.avatar')

    name.innerHTML = data.name
    avatar.setAttribute('src', data.avatar_url)
  })

console.log(process.env.USER)